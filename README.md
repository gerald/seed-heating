# Seed heating

Our seeds need the right temperature! This repository contains documentation and code for making sure our seeds always have the right temperature.

## Bill of materials

- A Raspberry Pi (or any other device which has two USB ports and runs linux)
- A TemPer temperature probe
- A Rutenbeck TCR IP 4 (or its smaller sibling with just a single channel)
- A USB ethernet adapter

## Preparations

- Disable DHCP on the TCR IP 4 and set a static ip and subnet (10.0.0.3, 255.255.255.0)
- On Raspbian add configuration for your usb ethernet adapter to `/etc/network/interfaces` (and the internal one, because adding something to the `interfaces` file breaks `dhcpd`)

      auto eth0
      allow-hotplug eth0
      iface eth0 inet dhcp

      auto eth1
      allow-hotplug eth1
      iface eth1 inet static
        address 10.0.0.1
        netmask 255.255.255.0


  Stop `dhcpd` with `sudo systemctl stop dhcpd`, then restart networking `sudo systemctl restart networking`

  Verify your usb adapter has a static ip by running `ip a`

- Plug in your TCR IP 4 and verify connectivity using `curl 10.0.0.3`
- Install `libusb-1.0-0-dev pkg-config` using `apt-get`
- Create `temper` group (`groupadd temper`)
- Add your user to the group (`usermod -aG temper pi`)
- Add file `/etc/udev/rules.d/60-temper.rules` with content

      # TEMPer1 USB thermometer
      ATTR{idVendor}=="0c45", ATTR{idProduct}=="7401", OWNER="root", GROUP="temper", MODE="0660"

- Reboot or execute `udevadm control --reload-rules` and replug the Temper probe

## Compilation

Due to C dependencies, compilation only works the target architecture

Install go >= 1.16 and run

```
./build.sh
```

## systemd service

The `seed-heating` binary can be run either inside a tmux session, or using the following systemd service.

This requires the following preparations

- Copy the `seed-heating` binary to `/usr/local/bin/seed-heating`
- Create directory `/etc/seed-heating`
- Create file `/etc/default/seed-heating` with content

```
TCR_HOST=10.0.0.3
LOWER_TEMP=22
UPPER_TEMP=27
```

- Create file `/etc/systemd/system/seed-heating.service` with content

```
[Unit]
Description=Seed Heating

[Service]
User=pi
Group=pi
EnvironmentFile=/etc/default/seed-heating
ExecStart=/usr/local/bin/seed-heating
TimeoutStopSec=5s
LimitNOFILE=1048576
LimitNPROC=512
PrivateTmp=true
ProtectSystem=full

[Install]
WantedBy=multi-user.target
```

- Execute `systemctl daemon-reload` everytime you change the `/etc/systemd/system/seed-heating.service` file
- Start and enable the service `systemctl enable seed-heating && systemctl start seed-heating`

## Optional Pi LED (mypifi.net rectangle)

| Color | GPIO | Board pin |
| --- | --- | --- |
| Red | 17  | 11  |
| Yellow | 27 | 13 |
| Green | 9 | 21 |


Example using sysfs

```
echo 9 >/sys/class/gpio/export
echo out >/sys/class/gpio/gpio9/direction
echo 1 >/sys/class/gpio/gpio9/value
```
