package main

import (
	"fmt"

	"github.com/ubergesundheit/temper-go"
)

func reconcile() error {
	var err error
	// read temperature
	logger.Println("Reading temperature")
	deviceTemp, err := temper.GetTemperature()
	if err != nil {
		return fmt.Errorf("Could not get temperature: %v", err)
	}
	currentTemp := deviceTemp.Temperature

	if currentTemp > lowerTempFlag && currentTemp < upperTempFlag {
		logger.Printf("Temperature %.2f °C is ok (Between %.2f and %.2f). Doing nothing\n", currentTemp, lowerTempFlag, upperTempFlag)
		// just check if heating is switched on, we're satisfied otherwise
		return nil
	}

	if currentTemp > upperTempFlag {
		logger.Printf("Temperature %.2f °C is too high. Switching heating OFF (limit %.2f)", currentTemp, upperTempFlag)
		err = tcrInstance.SwitchChannel(1, false)
		if err != nil {
			return fmt.Errorf("Error switching channel %d to OFF: %v", tcrChannel, err)
		}

		return nil
	}

	if currentTemp < lowerTempFlag {
		logger.Printf("Temperature %.2f °C is too low. Switching heating ON (limit %.2f)", currentTemp, lowerTempFlag)
		err = tcrInstance.SwitchChannel(1, true)
		if err != nil {
			return fmt.Errorf("Error switching channel %d to ON: %v", tcrChannel, err)
		}

		return nil
	}

	return nil
}
