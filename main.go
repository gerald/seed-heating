package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"codeberg.org/gerald/tcrip4"
	"github.com/genuinetools/pkg/cli"
)

var (
	version   = "dev"
	gitCommit = "dev"

	tcrHostFlag   string
	lowerTempFlag float64
	upperTempFlag float64

	tcrChannel = 1

	tcrInstance *tcrip4.TCRIP4
	logger      *log.Logger
)

func validateFlags() error {
	if strings.TrimSpace(tcrHostFlag) == "" {
		// retry with env var
		tcrHostFlag = os.Getenv("TCR_HOST")
		if strings.TrimSpace(tcrHostFlag) == "" {
			return fmt.Errorf("TCR IP 4 host cannot be empty")
		}
	}

	var err error
	if lowerTempFlag <= 0 {
		// retry with env var
		tempFlag := os.Getenv("LOWER_TEMP")
		lowerTempFlag, err = strconv.ParseFloat(tempFlag, 64)
		if err != nil {
			return fmt.Errorf("Error parsing lower temperature from env var LOWER_TEMP: %v", err)
		}
		if lowerTempFlag <= 0 {
			return fmt.Errorf("lower temperature flag must be set")
		}
	}

	if upperTempFlag <= 0 {
		// retry with env var
		tempFlag := os.Getenv("UPPER_TEMP")
		upperTempFlag, err = strconv.ParseFloat(tempFlag, 64)
		if err != nil {
			return fmt.Errorf("Error parsing upper temperature from env var UPPER_TEMP: %v", err)
		}
		if upperTempFlag <= 0 {
			return fmt.Errorf("upper temperature flag must be set")
		}
	}

	if upperTempFlag < lowerTempFlag {
		return fmt.Errorf("lower temperature flag (%f) must be lower than upper temperature flag (%f)", lowerTempFlag, upperTempFlag)
	}

	return nil
}

func main() {
	// Create a new cli program.
	p := cli.NewProgram()
	p.Name = "seed-heating"
	p.Description = "A tool to switch a heating mat depending on temperature"

	// Set the GitCommit and Version.
	p.GitCommit = gitCommit
	p.Version = version

	p.FlagSet = flag.NewFlagSet("global", flag.ExitOnError)
	p.FlagSet.StringVar(&tcrHostFlag, "a", "", "Host of the TCR IP 4 (or env var TCR_HOST)")
	p.FlagSet.Float64Var(&lowerTempFlag, "l", 0, "Enable heating at this temperature (celsius). Flag value has precedence. (env var LOWER_TEMP)")
	p.FlagSet.Float64Var(&upperTempFlag, "u", 0, "Disable heating at this temperature (celsius). Flag value has precedence. (env var UPPER_TEMP)")

	// Set the before function.
	p.Before = func(ctx context.Context) error {
		var err error
		err = validateFlags()
		if err != nil {
			return fmt.Errorf("Invalid flags: %v", err)
		}

		tcrInstance, err = tcrip4.NewTCRIP4(strings.TrimSpace(tcrHostFlag), "", "")
		if err != nil {
			return fmt.Errorf("Error initializing TCRIP4: %v", err)
		}

		logger = log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)

		return nil
	}

	// Set the main program action.
	p.Action = func(ctx context.Context, args []string) error {
		// On ^C, or SIGTERM handle exit.
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		signal.Notify(c, syscall.SIGTERM)
		go func() {
			for sig := range c {
				log.Printf("Received %s, exiting.", sig.String())
				os.Exit(0)
			}
		}()

		logger.Printf("Seed heating version %s (%s)\nC", version, gitCommit)
		logger.Println("---")
		logger.Printf("TCR IP 4 Host: %s\n", tcrHostFlag)
		logger.Printf("Lower temperature: %.2f\n", lowerTempFlag)
		logger.Printf("Upper temperature: %.2f\n", upperTempFlag)
		logger.Println("---")

		logger.Println("Starting reconciliation loop")

		sleepDuration := 5 * time.Second
		ticker := time.Tick(sleepDuration)
		var err error
		logger.Printf("Sleeping for %s\n", sleepDuration)
		for range ticker {
			err = reconcile()
			if err != nil {
				logger.Printf("Error reconciling: %v", err)
			}
			logger.Printf("Sleeping for %s\n", sleepDuration)
		}

		return nil
	}

	// Run our program.
	p.Run()
}
