#!/bin/bash

gitCommit="$(git rev-parse --short HEAD)"
version="$(git tag --points-at HEAD) (created $(git log -1 --format=%aI), built at $(date -Iseconds))"

GOOS=linux GOARCH=arm GOARM=6 go build -o seed-heating -ldflags="-s -w -X main.gitCommit=$gitCommit -X 'main.version=$version'" *.go
