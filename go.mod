module codeberg.org/gerald/heating-control

go 1.16

require (
	codeberg.org/gerald/tcrip4 v0.2.1
	github.com/genuinetools/pkg v0.0.0-20181022210355-2fcf164d37cb
	github.com/ubergesundheit/temper-go v0.0.0-20210220171200-09fc3fbf649c
)
